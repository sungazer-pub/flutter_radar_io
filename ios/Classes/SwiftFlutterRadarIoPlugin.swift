import Flutter
import UIKit
import RadarSDK

public class SwiftFlutterRadarIoPlugin: NSObject, FlutterPlugin {
    private static let METHOD_INITIALIZE = "initialize";
    private static let METHOD_SET_USER_ID = "set_user_id";
    private static let METHOD_GET_USER_ID = "get_user_id";
    private static let METHOD_SET_METADATA = "set_metadata";
    private static let METHOD_GET_METADATA = "get_metadata";
    private static let METHOD_SET_DESCRIPTION = "set_description";
    private static let METHOD_GET_DESCRIPTION = "get_description";
    private static let METHOD_TRACK_ONCE = "track_once";
    private static let METHOD_START_TRACKING = "start_tracking";
    private static let METHOD_STOP_TRACKING = "stop_tracking";
    private static let METHOD_IS_TRACKING = "is_tracking";
    private static let METHOD_SET_LOG_LEVEL = "set_log_level";
  
    private static let TRACKING_PRESET_EFFICIENT = "efficient";
    private static let TRACKING_PRESET_RESPONSIVE = "responsive";
    private static let TRACKING_PRESET_CONTINUOUS = "continuous";


    private static let LOG_LEVEL_ERROR = "log_level_error";
    private static let LOG_LEVEL_NONE = "log_level_none";
    private static let LOG_LEVEL_WARNING = "log_level_warning";
    private static let LOG_LEVEL_INFO = "log_level_info";
    private static let LOG_LEVEL_DEBUG = "log_level_debug";
  
    private static var channel: FlutterMethodChannel?;
    
  public static func register(with registrar: FlutterPluginRegistrar) {
    channel = FlutterMethodChannel(name: "flutter_radar_io", binaryMessenger: registrar.messenger())
    let instance = SwiftFlutterRadarIoPlugin()
    registrar.addMethodCallDelegate(instance, channel: channel!)
  }

  public func handle(_ call: FlutterMethodCall, result: @escaping FlutterResult) {
    switch (call.method) {
    case SwiftFlutterRadarIoPlugin.METHOD_INITIALIZE:
            let arguments = call.arguments as! [String: Any]
              let publishableKey = arguments["publishableKey"]  as! String;
              Radar.initialize(publishableKey: publishableKey)
            case SwiftFlutterRadarIoPlugin.METHOD_SET_LOG_LEVEL:
                    let arguments = call.arguments as! [String: Any]
              let logLevel = arguments["logLevel"]  as! String;
              switch (logLevel) {
                case SwiftFlutterRadarIoPlugin.LOG_LEVEL_DEBUG:
                    Radar.setLogLevel(.debug)
                case SwiftFlutterRadarIoPlugin.LOG_LEVEL_ERROR:
                    Radar.setLogLevel(.error)
                case SwiftFlutterRadarIoPlugin.LOG_LEVEL_INFO:
                    Radar.setLogLevel(.info)
                case SwiftFlutterRadarIoPlugin.LOG_LEVEL_NONE:
                    Radar.setLogLevel(.none)
                case SwiftFlutterRadarIoPlugin.LOG_LEVEL_WARNING:
                    Radar.setLogLevel(.warning)
                default:
                    Radar.setLogLevel(.none)
              }
              result(true);
            case SwiftFlutterRadarIoPlugin.METHOD_SET_USER_ID:
                    let arguments = call.arguments as! [String: Any]
              let userId = arguments["userId"]  as! String;
              Radar.setUserId(userId)
              result(Radar.getUserId())
            case SwiftFlutterRadarIoPlugin.METHOD_GET_USER_ID:
              let userId = Radar.getUserId()
              result(userId)
            case SwiftFlutterRadarIoPlugin.METHOD_SET_METADATA:
              let metadata = call.arguments as! [AnyHashable : Any];
              Radar.setMetadata(metadata)
              result(true)
            case SwiftFlutterRadarIoPlugin.METHOD_GET_METADATA:
              let metadata = Radar.getMetadata()
              result(metadata)
            case SwiftFlutterRadarIoPlugin.METHOD_SET_DESCRIPTION:
                    let arguments = call.arguments as! [String: Any]
              let description = arguments["description"]  as! String;
              Radar.setDescription(description)
              result(true)
            case SwiftFlutterRadarIoPlugin.METHOD_GET_DESCRIPTION:
              let description = Radar.getDescription()
              result(description)
            case SwiftFlutterRadarIoPlugin.METHOD_TRACK_ONCE:
                Radar.trackOnce { (status: RadarStatus, location: CLLocation?, events: [RadarEvent]?, user: RadarUser?) in
                    let args: NSDictionary = [
                        "radarStatus" : Radar.stringForStatus(status),
                        "location" : location,
                        "radarEvents" : events,
                        "radarUser" : user
                    ]
                    SwiftFlutterRadarIoPlugin.channel!.invokeMethod("callback", arguments: args);
                  // do something with location, events, user
                }
            case SwiftFlutterRadarIoPlugin.METHOD_START_TRACKING:
                    let arguments = call.arguments as! [String: Any]
                let preset = arguments["preset"]  as? String;
                var options = RadarTrackingOptions.responsive
                
                guard let localPreset = preset else {
                    Radar.startTracking(trackingOptions: arguments as! RadarTrackingOptions);
                    result(true);
                    return;
                }
                switch (preset!) {
                case SwiftFlutterRadarIoPlugin.TRACKING_PRESET_RESPONSIVE:
                    options = RadarTrackingOptions.responsive
                case SwiftFlutterRadarIoPlugin.TRACKING_PRESET_EFFICIENT:
                    options = RadarTrackingOptions.efficient
                case SwiftFlutterRadarIoPlugin.TRACKING_PRESET_CONTINUOUS:
                    options = RadarTrackingOptions.continuous
                default:
                    options = RadarTrackingOptions.efficient
                }
                Radar.startTracking(trackingOptions: options)
              result(true);
            case SwiftFlutterRadarIoPlugin.METHOD_STOP_TRACKING:
              Radar.stopTracking()
              result(true)
            case SwiftFlutterRadarIoPlugin.METHOD_IS_TRACKING:
              let isTracking = Radar.isTracking()
              result(isTracking)
            default:
              result(FlutterMethodNotImplemented)
          }
  }
}
