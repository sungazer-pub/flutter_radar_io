package io.sungazer.flutter_radar_io;

import android.Manifest;
import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.location.Location;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.flutter.embedding.engine.plugins.FlutterPlugin;
import io.flutter.embedding.engine.plugins.activity.ActivityAware;
import io.flutter.embedding.engine.plugins.activity.ActivityPluginBinding;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugin.common.MethodChannel.MethodCallHandler;
import io.flutter.plugin.common.MethodChannel.Result;
import io.flutter.plugin.common.PluginRegistry.Registrar;

import io.radar.sdk.Radar;
import io.radar.sdk.RadarTrackingOptions;
import io.radar.sdk.model.RadarEvent;
import io.radar.sdk.model.RadarUser;

/** FlutterRadarIoPlugin */
public class FlutterRadarIoPlugin implements FlutterPlugin, MethodCallHandler, ActivityAware {
  /// The MethodChannel that will the communication between Flutter and native Android
  ///
  /// This local reference serves to register the plugin with the Flutter Engine and unregister it
  /// when the Flutter Engine is detached from the Activity
  private MethodChannel channel;
  private Activity activity;

  private static final String METHOD_INITIALIZE = "initialize";
  private static final String METHOD_SET_USER_ID = "set_user_id";
  private static final String METHOD_GET_USER_ID = "get_user_id";
  private static final String METHOD_SET_METADATA = "set_metadata";
  private static final String METHOD_GET_METADATA = "get_metadata";
  private static final String METHOD_SET_DESCRIPTION = "set_description";
  private static final String METHOD_GET_DESCRIPTION = "get_description";
  private static final String METHOD_TRACK_ONCE = "track_once";
  private static final String METHOD_START_TRACKING = "start_tracking";
  private static final String METHOD_STOP_TRACKING = "stop_tracking";
  private static final String METHOD_IS_TRACKING = "is_tracking";
  private static final String METHOD_SET_LOG_LEVEL = "set_log_level";
  //private static final String METHOD_REQUEST_PERMISSIONS = "request_permissions";

  private static final String TRACKING_PRESET_EFFICIENT = "efficient";
  private static final String TRACKING_PRESET_RESPONSIVE = "responsive";
  private static final String TRACKING_PRESET_CONTINUOUS = "continuous";


  private static final String LOG_LEVEL_ERROR = "log_level_error";
  private static final String LOG_LEVEL_NONE = "log_level_none";
  private static final String LOG_LEVEL_WARNING = "log_level_warning";
  private static final String LOG_LEVEL_INFO = "log_level_info";
  private static final String LOG_LEVEL_DEBUG = "log_level_debug";

  private Context context;

  private void setContext(Context context) {
    this.context = context;
  }

  @Override
  public void onAttachedToActivity(ActivityPluginBinding activityPluginBinding) {
    this.activity = activityPluginBinding.getActivity();
  }

  @Override
  public void onDetachedFromActivityForConfigChanges() {
    this.activity = null;
  }

  @Override
  public void onReattachedToActivityForConfigChanges(ActivityPluginBinding activityPluginBinding) {
    this.activity = activityPluginBinding.getActivity();
  }

  @Override
  public void onDetachedFromActivity() {
    this.activity = null;
  }

  @Override
  public void onAttachedToEngine(@NonNull FlutterPluginBinding flutterPluginBinding) {
    this.context = flutterPluginBinding.getApplicationContext();
    channel = new MethodChannel(flutterPluginBinding.getFlutterEngine().getDartExecutor(), "flutter_radar_io");
    channel.setMethodCallHandler(this);
  }

  // This static function is optional and equivalent to onAttachedToEngine. It supports the old
  // pre-Flutter-1.12 Android projects. You are encouraged to continue supporting
  // plugin registration via this function while apps migrate to use the new Android APIs
  // post-flutter-1.12 via https://flutter.dev/go/android-project-migration.
  //
  // It is encouraged to share logic between onAttachedToEngine and registerWith to keep
  // them functionally equivalent. Only one of onAttachedToEngine or registerWith will be called
  // depending on the user's project. onAttachedToEngine or registerWith must both be defined
  // in the same class.
  public static void registerWith(Registrar registrar) {
    final MethodChannel channel = new MethodChannel(registrar.messenger(), "flutter_radar_io");
    final FlutterRadarIoPlugin plugin = new FlutterRadarIoPlugin();
    plugin.setContext(registrar.context());
    channel.setMethodCallHandler(plugin);
  }

  @Override
  public void onMethodCall(@NonNull MethodCall call, @NonNull Result result) {
    try {
      switch (call.method) {
//        case METHOD_REQUEST_PERMISSIONS:
//        {
//          ActivityCompat.requestPermissions(activity, new String[] { Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_BACKGROUND_LOCATION }, requestCode);
//          break;
//        }
        case METHOD_INITIALIZE:
        {
          final String publishableKey = call.argument("publishableKey");
          Radar.initialize(this.context, publishableKey);
          Radar.setLogLevel(Radar.RadarLogLevel.DEBUG);
          result.success(true);
          break;
        }
        case METHOD_SET_LOG_LEVEL:
        {
          final String logLevel = call.argument("logLevel");
          switch (logLevel) {
            case LOG_LEVEL_DEBUG:
              Radar.setLogLevel(Radar.RadarLogLevel.DEBUG);
              break;
            case LOG_LEVEL_ERROR:
              Radar.setLogLevel(Radar.RadarLogLevel.ERROR);
              break;
            case LOG_LEVEL_INFO:
              Radar.setLogLevel(Radar.RadarLogLevel.INFO);
              break;
            case LOG_LEVEL_NONE:
              Radar.setLogLevel(Radar.RadarLogLevel.NONE);
              break;
            case LOG_LEVEL_WARNING:
              Radar.setLogLevel(Radar.RadarLogLevel.WARNING);
              break;
            default:
              Radar.setLogLevel(Radar.RadarLogLevel.NONE);
              break;
          }
          result.success(true);
          break;
        }
        case METHOD_SET_USER_ID:
        {
          final String userId = call.argument("userId");
          Radar.setUserId(userId);
          result.success(Radar.getUserId());
          break;
        }
        case METHOD_GET_USER_ID:
        {
          final String userId = Radar.getUserId();
          result.success(userId);
          break;
        }
        case METHOD_SET_METADATA:
        {
          final JSONObject metadata = (JSONObject) call.arguments;
          Radar.setMetadata(metadata);
          result.success(true);
          break;
        }
        case METHOD_GET_METADATA:
        {
          final JSONObject metadata = Radar.getMetadata();
          result.success(metadata);
          break;
        }
        case METHOD_SET_DESCRIPTION:
        {
          final String description = call.argument("description");
          Radar.setDescription(description);
          break;
        }
        case METHOD_GET_DESCRIPTION:
        {
          final String description = Radar.getDescription();
          result.success(description);
          break;
        }
        case METHOD_TRACK_ONCE:
        {
          Radar.trackOnce(
                  new Radar.RadarTrackCallback() {
                    @Override
                    public void onComplete(@NotNull final Radar.RadarStatus radarStatus, @Nullable final Location location, @Nullable final RadarEvent[] radarEvents, @Nullable final RadarUser radarUser) {
                      try{
                        activity.runOnUiThread(new Runnable() {
                          @Override
                          public void run() {
                            ObjectMapper oMapper = new ObjectMapper();
                            Map<String, Object> args = new HashMap<>();
                            args.put("radarStatus", radarStatus.toString());
                            args.put("location", oMapper.convertValue(location, Map.class));
                            if (radarEvents != null && radarEvents.length > 0) {
                              List<Map<String, Object>> events = new ArrayList<Map<String, Object>>();
                              for (RadarEvent radarEvent : radarEvents) {
                                events.add(oMapper.convertValue(radarEvent, Map.class));
                              }
                              args.put("radarEvents", oMapper.convertValue(events, Map.class));
                            }
                            args.put("radarUser", oMapper.convertValue(radarUser, Map.class));
                            channel.invokeMethod("callback", args);
                          }
                        });
                      }catch(Exception e){
                        Log.d("Radar", e.getMessage());
                      }
                    }
                  }
          );
          break;
        }
        case METHOD_START_TRACKING:
        {
          final String preset = call.argument("preset");
          RadarTrackingOptions options = null;
          if(preset != null) {
            switch (preset) {
              case TRACKING_PRESET_RESPONSIVE:
                options = RadarTrackingOptions.RESPONSIVE;
                break;
              case TRACKING_PRESET_EFFICIENT:
                options = RadarTrackingOptions.EFFICIENT;
                break;
              case TRACKING_PRESET_CONTINUOUS:
                options = RadarTrackingOptions.CONTINUOUS;
                break;
            }
          } else {
            options = (RadarTrackingOptions) call.arguments;
          }
          assert options != null;
          Radar.startTracking(options);
          result.success(true);
          break;
        }
        case METHOD_STOP_TRACKING:
        {
          Radar.stopTracking();
          result.success(true);
          break;
        }
        case METHOD_IS_TRACKING: {
          final boolean isTracking = Radar.isTracking();
          result.success(isTracking);
          break;
        }
        default:
          result.notImplemented();
      }
    } catch (Error e) {
      result.error(e.toString(), e.getMessage(), e.getCause());
    }
  }

  @Override
  public void onDetachedFromEngine(@NonNull FlutterPluginBinding binding) {
    this.context = null;
    channel.setMethodCallHandler(null);
  }
}
