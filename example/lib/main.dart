import 'package:flutter/material.dart';
import 'dart:async';

import 'package:flutter/services.dart';
import 'package:flutter_radar_io/flutter_radar_io.dart';
import 'package:permission_handler/permission_handler.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  String userId;
  bool isTracking;
  String status;

  @override
  void initState() {
    super.initState();
  }

  // Platform messages are asynchronous, so we initialize in an async method.
  Future<void> initPlatformState() async {
    try {
    } on PlatformException {

    }
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Plugin example app'),
        ),
        body: Center(
          child: Column(
            children: [
              Text("Tracking status: ${isTracking}"),
              Text("TrackOnce status: ${status}"),
              MaterialButton(
                child: Text('Request permissions'),
                onPressed: () async {
                  try {
                    await Permission.locationAlways.request();
                  } on PlatformException {
                    print('There has been an error');
                  }
                },
              ),
              MaterialButton(
                child: Text('Initialize'),
                onPressed: () async {
                  try {
                    await FlutterRadarIo.initialize(publishableKey: 'YOUR_KEY');
                  } on PlatformException {
                    print('There has been an error');
                  }
                },
              ),
              MaterialButton(
                child: Text('Set Log Level'),
                onPressed: () async {
                  try {
                    await FlutterRadarIo.setLogLevel(logLevel: FlutterRadarIo.LOG_LEVEL_WARNING);
                  } on PlatformException {
                    print('There has been an error');
                  }
                },
              ),
              MaterialButton(
                child: Text('Start Tracking'),
                onPressed: () async {
                  try {
                    await FlutterRadarIo.startTracking(preset: FlutterRadarIo.TRACKING_PRESET_CONTINUOUS);
                  } on PlatformException {
                    print('There has been an error');
                  }
                },
              ),
              MaterialButton(
                child: Text('Stop Tracking'),
                onPressed: () async {
                  try {
                    await FlutterRadarIo.stopTracking();
                  } on PlatformException {
                    print('There has been an error');
                  }
                },
              ),
              MaterialButton(
                child: Text('Check if tracking'),
                onPressed: () async {
                  try {
                    final bool tracking = await FlutterRadarIo.isTracking();
                    setState(() {
                      isTracking = tracking;
                    });
                  } on PlatformException {
                    print('There has been an error');
                  }
                },
              ),
              MaterialButton(
                child: Text('Track Once'),
                onPressed: () async {
                  try {
                    await FlutterRadarIo.trackOnce(
                            callBack: ({location, radarEvents, radarStatus, radarUser}) {
                              print('callback executing');
                              setState(() {
                                status = radarStatus;
                              });
                              print([radarStatus, location, radarEvents, radarUser]);
                            },);
                  } on Exception {
                    print('There has been an error');
                  }
                },
              )
            ],
          )
        ),
      ),
    );
  }
}
