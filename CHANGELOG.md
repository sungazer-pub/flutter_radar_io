## 0.1.0

* Implemented bidirectional communication for trackOnce on Android

## 0.0.2

* Fixed iOS build

## 0.0.1+1

* Fixed Readme.md

## 0.0.1

* Initial Release.
